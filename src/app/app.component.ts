import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  posts = [
  	{
  		titre: "Mon premier post",
  		contenu: "Salut c'est mooiiiii"
  	},
  	{
  		titre: "Mon deuxième post",
  		contenu: "En fait c'était pas moi"
  	},
  	{
  		titre: "Encore un post",
  		contenu: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eget imperdiet est. Fusce sed dictum nulla. Nullam quam risus, porttitor sed tempus non, facilisis nec lorem. Sed ac dui pellentesque, gravida eros et, aliquet tortor. Integer vulputate tincidunt rutrum. Pellentesque semper purus ac mauris dapibus accumsan. Cras porttitor nulla rutrum, rutrum augue nec, feugiat lacus. Integer a tempus arcu, sit amet tincidunt ex. Vestibulum sit amet erat magna. Morbi ac viverra arcu. Nulla ultricies mattis posuere. Vestibulum placerat aliquam nisl. Nullam dolor leo, condimentum et feugiat id, rhoncus et nisi. Morbi tempor eleifend tempor."
  	}
  ];
}
