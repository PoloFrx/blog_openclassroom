import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.scss']
})
export class PostListComponentComponent implements OnInit {
	@Input() postTitle: string;
	@Input() postContent: string;
	postLikes: number = 0;
	postDislikes: number = 0;

	creationDate = new Date();

  constructor() { }

  ngOnInit() {
  }

  upLike() {
  	this.postLikes++;
  }

  upDislike() {
  	this.postDislikes++;
  }

}
